cvit包中

声明整型变量x，
构造方法实现以下功能，为x赋值为4，打印输出"in SuperClass : x = [x的值]"
定义方法aMethod()，打印输出"in superClass.aMethod()"

package com.cvit;

public class Superclass {
	int x;
	public Superclass(){
		x=4;
		System.out.println("in Superclass : x ="+x);
	
	}
	public void aMethod(){
		System.out.println("in SuperClass.aMethod()");
	}

}



4. 创建类SubClass1，继承自SuperClass，封装在com.cvit包中

声明整型变量x，
构造方法实现以下功能，为x赋值为6，打印输出"in subClass : x = [x的值]"
定义方法aMethod()
调用父类的构造方法；
打印输出"in SubClass1.aMethod()"；
打印输出"super.x = [父类中x的值], sub.x=[子类中x的值]"；
定义方法aMethod(int a, int b)，返回a+b的值


package com.cvit;

public class SubClass1 extends Superclass{
	int x;
	public SubClass1 (){
		x=6;
		System.out.println("in subclass : x = "+x);
	}
	public void aMethod(){
		System.out.println("in Subclass1.aMethod()");
		System.out.println("super.x = "+super.x+"sub.x="+x);
	}
	public int aMethod(int a,int b){
		return a+b;
	}

}


5. 创建类SubClass2，继承自SuperClass，封装在com.cvit包中

定义方法aMethod()，打印输出"in SubClass2.aMethod()"
定义方法aMethod(int a, int b)，返回a+b的值

package com.cvit;

public class SubClass2 extends Superclass{
	public void aMethod(){
		System.out.println("in Subclass2.aMethod()");
	}
	public int aMethod(int a,int b){
		return a+b;
	}

}


6. 创建类SubClass3，继承自SuperClass，封装在com.cvit包中

定义方法aMethod()，打印输出"in SubClass3.aMethod()"
定义方法aMethod(int a, int b)，返回a+b的值

package com.cvit;

public class SubClass3 extends Superclass{
	public void aMethod(){
		System.out.println("in Subclass3.aMethod()");
	}
	public int aMethod(int a,int b){
		return a+b;
	}

}


7. 创建主类Main，封装在com.cvit包中

定义返回值为空的静态方法chooseClass(SubClass2 sub2)，调用SubClass2的aMethod方法
定义返回值为空的静态方法chooseClass(SubClass3 sub3)，调用SubClass3的aMethod方法
程序的入口方法实现
定义整型变量x，赋值为7；
创建Airplane类的对象airplane；
调用Airplane类的takeOff方法；
创建SubClass1类的对象sub1；
调用SubClass1类的aMethod方法；
调用SubClass1类的aMethod方法，传值(x, x)，将得到的结果赋值给x；
调用Airplane类的fly方法；
创建SubClass2类的对象sub2；
创建SubClass3类的对象sub3；
调用chooseClass方法，将sub2作为参数传入；
调用SubClass2类的aMethod方法，传值(x, x)，将得到的结果赋值给x；
调用chooseClass方法，将sub3作为参数传入；
调用SubClass3类的aMethod方法，传值(x, x)，将得到的结果赋值给x；
打印输入"x = [x的值]"；
调用Airplane类的land方法；



package com.cvit;

public class Main {

	/**
	 * @param args
	 */
	public static void chooseClass(SubClass2 sub2){
		sub2.aMethod();
	}
	public static void chooseClass(SubClass3 sub3){
		sub3.aMethod();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x=7;
		Airplane airplane = new Airplane();
		airplane.takeoff();
		SubClass1 sub1  = new SubClass1();
		sub1.aMethod();
		sub1.aMethod(x,x);
		airplane.fly();
		SubClass2 sub2  = new SubClass2();
		SubClass3 sub3  = new SubClass3();
		Main.chooseClass(sub2);
		x=sub2.aMethod(x,x);
		Main.chooseClass(sub3);
		x=sub3.aMethod(x,x);
		System.out.println("x ="+x);
		airplane.land();
		
	}

}